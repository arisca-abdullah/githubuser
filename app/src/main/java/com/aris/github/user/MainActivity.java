package com.aris.github.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private UserAdapter adapter;
    private TypedArray dataPhoto;
    private String[] dataName;
    private String[] dataUsername;
    private String[] dataLocation;
    private String[] dataCompanies;
    private int[] dataRepositories;
    private int[] dataFollowers;
    private int[] dataFollowing;
    private ArrayList<User> users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Github Users");
        }

        ListView listView = findViewById(R.id.lv_users);
        adapter = new UserAdapter(this);
        listView.setAdapter(adapter);

        prepare();
        addItem();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent moveWithObjectIntent = new Intent(MainActivity.this, DetailActivity.class);
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_USER, users.get(position));
                startActivity(moveWithObjectIntent);
            }
        });
    }

    private void prepare() {
        dataPhoto = getResources().obtainTypedArray(R.array.avatar);
        dataName = getResources().getStringArray(R.array.name);
        dataUsername = getResources().getStringArray(R.array.username);
        dataLocation = getResources().getStringArray(R.array.location);
        dataCompanies = getResources().getStringArray(R.array.company);
        dataRepositories = getResources().getIntArray(R.array.repository);
        dataFollowers = getResources().getIntArray(R.array.followers);
        dataFollowing = getResources().getIntArray(R.array.following);
    }

    private void addItem() {
        users = new ArrayList<>();

        for (int i = 0; i < dataName.length; i++) {
            User user = new User();
            user.setPhoto(dataPhoto.getResourceId(i, -1));
            user.setName(dataName[i]);
            user.setUsername(dataUsername[i]);
            user.setLocation(dataLocation[i]);
            user.setCompany(dataCompanies[i]);
            user.setRepositories(dataRepositories[i]);
            user.setFollowers(dataFollowers[i]);
            user.setFollowing(dataFollowing[i]);

            users.add(user);
        }

        adapter.setUsers(users);
    }
}
