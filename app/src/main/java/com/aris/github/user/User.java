package com.aris.github.user;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    private int Photo;
    private String name;
    private String username;
    private String location;
    private String company;
    private int repositories;
    private int followers;
    private int following;

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public User() {

    }

    protected User(Parcel in) {
        Photo = in.readInt();
        name = in.readString();
        username = in.readString();
        location = in.readString();
        company = in.readString();
        repositories = in.readInt();
        followers = in.readInt();
        following = in.readInt();
    }

    public int getPhoto() {
        return Photo;
    }

    public void setPhoto(int photo) {
        Photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getRepositories() {
        return repositories;
    }

    public void setRepositories(int repositories) {
        this.repositories = repositories;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Photo);
        dest.writeString(name);
        dest.writeString(username);
        dest.writeString(location);
        dest.writeString(company);
        dest.writeInt(repositories);
        dest.writeInt(followers);
        dest.writeInt(following);
    }

}
