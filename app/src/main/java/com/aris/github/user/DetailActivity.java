package com.aris.github.user;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_USER = "extra_user";

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Detail User");
        }

        CircleImageView imgUser = findViewById(R.id.img_detail_user);
        TextView tvName = findViewById(R.id.tv_detail_name);
        TextView tvUsername = findViewById(R.id.tv_username);
        TextView tvCompany = findViewById(R.id.tv_company);
        TextView tvLocation = findViewById(R.id.tv_location);
        TextView tvRepositories = findViewById(R.id.tv_repositories);
        TextView tvFollowers = findViewById(R.id.tv_followers);
        TextView tvFollowing = findViewById(R.id.tv_following);

        User user = getIntent().getParcelableExtra(EXTRA_USER);

        Glide.with(this)
                .load(user.getPhoto())
                .into(imgUser);
        tvName.setText(user.getName());
        tvUsername.setText(String.format("@%s", user.getUsername()));
        tvCompany.setText(user.getCompany());
        tvLocation.setText(user.getLocation());
        tvRepositories.setText(String.format("%d repositories", user.getRepositories()));
        tvFollowers.setText(String.format("%d followers", user.getFollowers()));
        tvFollowing.setText(String.format("%d following", user.getFollowing()));
    }

}
