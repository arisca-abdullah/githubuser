package com.aris.github.user;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<User> users = new ArrayList<>();

    public UserAdapter(Context context) {
        this.context = context;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false);
        }

        ViewHolder viewHolder = new ViewHolder(convertView);

        User user = (User) getItem(position);
        viewHolder.bind(user);
        return  convertView;
    }

    private class ViewHolder {
        private CircleImageView imgPhoto;
        private TextView tvName;
        private TextView tvLocation;

        ViewHolder(View view) {
            imgPhoto = view.findViewById(R.id.img_user);
            tvName = view.findViewById(R.id.tv_name);
            tvLocation = view.findViewById(R.id.tv_location);
        }

        void bind(User user) {
            Glide.with(context)
                    .load(user.getPhoto())
                    .into(imgPhoto);
            tvName.setText(user.getName());
            tvLocation.setText(user.getLocation());
        }
    }
}
